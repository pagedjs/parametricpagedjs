import pagedMakerConfig from './modules/config.js'
import { delay, print } from './modules/utils.js'
import { getValue, getMargins, getPageSize } from './modules/getValues.js'
import styles from './modules/baseStylesheet.js'
import pagedmakerCSS from './modules/pagedmakerCSS.js'
import uicss from './modules/uicss.js'
import ui from './modules/ui.js'
import config from './modules/config.js'
// create the UI and push it to the browser

// trying event delegation

window.onload = document.body.insertAdjacentHTML('afterbegin', ui)

document.querySelector('body').addEventListener('click', function(e) {
  if (
    e.target === document.querySelector(`${pagedMakerConfig.button}`) ||
    e.target.classList.contains('pagedPreviewer-button-close')
  ) {
    toggleModal(document.querySelector('.pagedPreviewer-modal'))
  }
})

// add styles for the preview box
const uicsscontent = document.createElement('style')
uicsscontent.textContent = uicss
document.head.append(uicsscontent)

// inputs for the style sheet
const inputs = document.querySelectorAll('.cssVarUpdate')
const selects = document.querySelectorAll('select')

//reload on value changes in the input
inputs.forEach((input) => {
  input.addEventListener('change', populatePagedMaker)
})

//reload on value changes in the selects
selects.forEach((select) => {
  select.addEventListener('change', populatePagedMaker)
})

// reload when changing the margins
document.querySelectorAll('.pagedPreviewer-marginUpdate').forEach((mr) => {
  mr.addEventListener('change', populatePagedMaker)
})

document
  .querySelector('.pagedPreviewer-button-preview')
  .addEventListener('click', populatePagedMaker)
document
  .querySelector('.pagedPreviewer-button-print')
  .addEventListener('click', print)

// show hide modal
function toggleModal(modal) {
  modal.classList.toggle('pagedPreviewer-hidden')
  document.body.classList.remove('pagedPreviewer-blocked')
  if (
    !document
      .querySelector('.pagedPreviewer-modal')
      .classList.contains('pagedPreviewer-hidden')
  ) {
    document.body.classList.add('pagedPreviewer-blocked')
    populatePagedMaker()
  }
}

// populate the iframe
async function populatePagedMaker() {
  // remove the previous iframe
  if (document.querySelector('.pagedPreviewer-previewFrame')) {
    document.querySelector('.pagedPreviewer-previewFrame').remove()
  }

  // create the iframe
  createFrame()

  // wait for the iframe to load the empty src before adding the content
  await delay(100)

  // add a pagedjs basic stylesheet for previewing the pages.
  let interfacecss = document.createElement('style')
  interfacecss.textContent = pagedmakerCSS

  document
    .querySelector('.pagedPreviewer-previewFrame')
    .contentDocument.head.appendChild(interfacecss)

  // create the script to the hooks

  //generate the hook
  // let hookblock = ''
  // console.log(config.hooks)
  // config.hooks.forEach((hook) => {
  //   hookblock = hookblock + `\n<scrip src="${hook}"></script>`
  // })
  // console.log(hookblock)
  //
  // document
  //   .querySelector('.pagedPreviewer-previewFrame')
  //   .contentDocument.head.insertAdjacentHTML(hookblock)

  // document
  //   .querySelector(".pagedPreviewer-previewFrame")
  //   .contentDocument.head.appendChild(hook);

  //  create a style element
  let styleElement = document
    .querySelector('.pagedPreviewer-previewFrame')
    .contentDocument.createElement('style')

  // populate the style elements that contains the updated styles
  styleElement.textContent = styles + getMargins() + getPageSize()
  console.log(getPageSize)

  // create a article element to paginate
  let articleContent = document
    .querySelector('.pagedPreviewer-previewFrame')
    .contentDocument.createElement('article')

  //find the content element from the config or use the body if undefined
  if (pagedMakerConfig.content == undefined) {
    articleContent.innerHTML = document.body.innerHTML
  } else {
    articleContent.innerHTML = document.querySelector(
      `${pagedMakerConfig.content}`
    ).innerHTML
  }

  // populate the article element of the iframe with the element found in the config
  document
    .querySelector('.pagedPreviewer-previewFrame')
    .contentDocument.body.appendChild(articleContent)

  // fetch data of each value:
  getAllValues(styleElement)

  // push the style element to the head of the iframe
  document
    .querySelector('.pagedPreviewer-previewFrame')
    .contentDocument.head.appendChild(styleElement)

  // import the css file coming from the configuration file (after the other styles)

  if (pagedMakerConfig.stylesheet != undefined) {
    // import the css file
    let stylesheetConf = document.createElement('link')
    stylesheetConf.href = `${pagedMakerConfig.stylesheet}`
    stylesheetConf.rel = 'stylesheet'
    document
      .querySelector('.pagedPreviewer-previewFrame')
      .contentDocument.head.appendChild(stylesheetConf)
  }

  // fetch and run pagedjs in the iframe
  let pagedjsscript = document.createElement('script')
  pagedjsscript.src = 'https://unpkg.com/pagedjs/dist/paged.polyfill.js'
  document
    .querySelector('.pagedPreviewer-previewFrame')
    .contentDocument.head.appendChild(pagedjsscript)
}

function getAllValues(styleElement) {
  // create an empty string value
  let values = ''

  //add pageSize

  // populate the value with the data for each select
  selects.forEach((input) => {
    if (!input.classList.contains('pagedSize')) {
      values = values + getValue(input)
    }
  })

  // populate the value with the data for each inputs
  inputs.forEach((input) => {
    values = values + getValue(input)
    console.log(values)
  })

  // set the css properties to the values
  values = `body {${values}}`

  // populate the styles element with all the values
  return (styleElement.textContent = styleElement.textContent + values)
}

// create iframe
function createFrame() {
  const pagedMaker = document.createElement('iframe')
  pagedMaker.classList.add('pagedPreviewer-previewFrame')
  document.querySelector('.pagedPreviewer-zonePreview').appendChild(pagedMaker)
}
