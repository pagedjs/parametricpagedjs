
async function delay(time) {
    return new Promise((resolve, reject) => {
      if (isNaN(time)) {
        reject(new Error("delay requires a valid number"));
      } else {
        setTimeout(resolve, time);
      }
    });
  }



function print(e) {
  e.preventDefault();
  var t = document.querySelector(".pagedPreviewer-previewFrame").contentWindow;
  t.focus(), t.print();
}


export {delay, print};

