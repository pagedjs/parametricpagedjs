import config from './config.js';

// style for the export that include css variable for the UI;
export default `

@import url('https://fonts.googleapis.com/css2?family=Spectral:ital,wght@0,400;0,500;1,400;1,500&display=swap');
@import url('https://fonts.googleapis.com/css2?family=Courier+Prime:ital,wght@0,400;0,700;1,400;1,700&display=swap');
@media print {

.pagedPreviewer-modal, .pagedPreviewer-button-modal {
    display: none;
}

body, html {
    margin: 0;
    padding: 0;
}

body { 
    ${config.features.includes("fontFamily")
        ? "font-family: var(--fontBody, sans-serif);"
        : ""
    }
    ${config.features.includes("fontSize")
        ? "font-size: var(--fontSize, 14px);"
        : ""
    }
    ${config.features.includes("lineHeight")
        ? "line-height: var(--lineHeight, 1.4);"
        : ""
    }
    ${config.features.includes("textColor")
        ? "color: var(--body-color);"
        : ""
    }

    
    
     
} 
 
@page { 
    ${config.features.includes("backgroundColor")
        ? "background: var(--background-color);"
        : ""
    }
    @bottom-right{
        content: counter(page) "/" counter(pages);
    }
    @bottom-left {
        content: string(text)
    }
}

title {
    string-set: title string(text);
}



h1,h2 { 
    
    text-transform: uppercase;
    border-bottom: 1px solid black;
}

}

`;
