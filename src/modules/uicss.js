import pagedMakerConfig from "./config.js";

export default `
@import url('https://fonts.googleapis.com/css2?family=Jost:wght@100&display=swap');



.pagedPreviewer-modal {
  color: black;
  font-family: sans-serif;
  font-weight:400;
  font-size: 18px;
  display: grid;
  position: fixed;
  margin: auto;
  top: 5vh;
  right: 5vw; 
  bottom: 5vh;
  left: 5vw;
  /* border: 2px solid black; */
  grid-template-columns: 1fr 5fr;
  padding: 0;
  background: white;
  z-index: 99999999999999999999999999999;
  background: white;
  border: 3px solid black;
  box-shadow: 0 0 0 10vw rgba(0,0,0,0.6);
  overflow-y: hidden;
}

.pagedPreviewer-modal .logo {
    height: 40px;
}

.pagedPreviewer-button-close {
    position: absolute;
    right: 2em;
    top: 2em;
}

.pagedPreviewer-hidden {
  display: none;
}

.pagedPreviewer-blocked {
     overflow: hidden;
}




.pagedPreviewer-item {
    margin-bottom: 2em;
}

.pagedPreviewer-modal label  {
    text-transform: uppercase;
    display: block;
}

.pagedPreviewer-preview {
}

.pagedPreviewer-previewFrame {
    border: none;
  height: 100%;
  width: 100%;
  max-width: unset;
  max-height: unset;
}

.pagedPreviewer-form {
    padding: 2em;
}

.pagedMaker-hyde {
  display: none;  
}
`;
