let config;

//hooks are not ready yet
export default config = window.pagedMakerConfig || { 
    content: "body",
    stylesheet: undefined,
    hooks: undefined,
    button: '.pagedPreviewer-button-modal',
    features: [
    "pageSize",
    "pageMargins",
    "fontFamily",
    "fontSize",
    "lineHeight",
    "backgroundColor",
    "textColor"
    ],
    uicolor: undefined,
  };


