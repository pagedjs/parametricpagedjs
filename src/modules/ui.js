import config from "./config.js";
import featuresList from "./features.js";
import logo from "./pagedjsLogo.js";

let features = "";



config.features.forEach((feat) => {
  featuresList.forEach((module) => {
    if (module.name == feat) {
      console.log('dang', module.name)
      features += module.render;
    } else {
        console.log(`${feat} is not existing in the list of modules`);
    }
  });
});


export default `
${
  !config.button
    ? '<button class="pagedPreviewer-button-modal">Start pagedjs</button>'
    : ""
} 

<div class="pagedPreviewer-modal pagedPreviewer-hidden">
    <div class="pagedPreviewer-form">
        <figure class="pagedPreviewer-logo">
            ${logo}
            <figcaption>paged.js <span>previewer</span></figcaption>
        </figure>
      
        ${features} 
    
     
        <div class="pagedPreviewer-item">
        <button class="pagedPreviewer-button-print">print</button>
    </div>
        <div class="pagedPreviewer-item">
        <button class="pagedPreviewer-button-preview">preview</button>
    </div>
    <div class="pagedPreviewer-item">
        <button class="pagedPreviewer-button-close">close</button>
    </div>
    </div>

    <div class="pagedPreviewer-zonePreview">
    
    </iframe>  
    </div>
</div>`;
