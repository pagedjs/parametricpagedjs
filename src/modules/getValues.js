function getValue(input) {
  const suffix = input.dataset.sizing || "";
  return `${input.dataset.cssProp}: ${input.value}${suffix};`;
}

function getMargins() {
  let marginObject = "@page { margin: ";
  document.querySelectorAll(".pagedPreviewer-marginUpdate").forEach((mr) => {
    marginObject = marginObject + ` ${mr.value + mr.dataset.sizing}`;
  });
  marginObject = marginObject + "}";
  return marginObject;
}

function getPageSize() {
  let pageSize = `@page { size: ${document.querySelector("select.pagedSize")?.value ? document.querySelector("select.pagedSize").value : 'letter'}; }`;
  return pageSize;
}


// this is not usable yet, as 3 margin-boxes are hidden
function getMarginBoxElement() {
  document
    .querySelector(".pagedPreviewer-previewFrame")
    .contentDocument.addEventListener("click", function (e) {

      // if (e.target.classList.contains("pagedjs_margin-top-left-corner")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-top-right-corner")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-bottom-left-corner")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-bottom-right-corner")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-top-left")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-top-center")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-top-right")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-bottom-left")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-bottom-center")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-bottom-right")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-left-top")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-left-middle")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-left-bottom")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-right-top")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-right-middle")) {
      //   console.log(e.target);
      // }
      // if (e.target.classList.contains("pagedjs_margin-right-bottom")) {
      //   console.log(e.target);
      // }
    });
}


export { getValue, getMargins, getPageSize, getMarginBoxElement};
