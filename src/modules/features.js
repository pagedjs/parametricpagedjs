import pageSize from './features/pageSize.js'
import pageMargins from "./features/pageMargins.js"
import fontFamily from './features/fontFamily.js'
import fontSize from './features/fontSize.js'
import lineHeight from './features/lineHeight.js'


let featureList = [fontFamily, pageSize, pageMargins, fontSize, lineHeight];

export default featureList;

