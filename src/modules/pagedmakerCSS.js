// interface preview with pages
export default `


/* Change the look */
:root{
    --color-background:  transparent;
    --color-pageBox: #666;
    --color-paper: white;
    --color-marginBox: transparent;
  }
  
  
  /* To define how the book look on the screen: */
  @media screen {
    body {
        background-color: var(--color-background);
    }
    .pagedjs_pages {
        display: flex;
        width: calc(var(--pagedjs-width) * 2);
        flex: 0;
        flex-wrap: wrap;
        margin: 0 auto;
    }
    .pagedjs_page {
        box-shadow: 0 0 0 1px var(--color-pageBox);
        margin: 0;
        flex-shrink: 0;
        flex-grow: 0;
        margin-top: 10mm;
    }
    .pagedjs_first_page {
        margin-left: var(--pagedjs-width);
    }
  
    .pagedjs_page:last-of-type{ 
        margin-bottom: 10mm;
    }
  
  
    /* show the margin-box */
  
    .pagedjs_margin-top-left-corner-holder,
    .pagedjs_margin-top,
    .pagedjs_margin-top-left,
    .pagedjs_margin-top-center,
    .pagedjs_margin-top-right,
    .pagedjs_margin-top-right-corner-holder,
    .pagedjs_margin-bottom-left-corner-holder,
    .pagedjs_margin-bottom,
    .pagedjs_margin-bottom-left,
    .pagedjs_margin-bottom-center,
    .pagedjs_margin-bottom-right,
    .pagedjs_margin-bottom-right-corner-holder,
    .pagedjs_margin-right,
    .pagedjs_margin-right-top,
    .pagedjs_margin-right-middle,
    .pagedjs_margin-right-bottom,
    .pagedjs_margin-left,
    .pagedjs_margin-left-top,
    .pagedjs_margin-left-middle,
    .pagedjs_margin-left-bottom{
        box-shadow: 0 0 0 1px inset var(--color-marginBox);
    }
  
  }
  
  
  
  
  
  @media screen {
  
      .pagedjs_pages {
          flex-direction: column;
          width: 100%;
      }
    
      .pagedjs_first_page {
          margin-left: 0;
      }
    
      .pagedjs_page {
          margin: 10mm auto 5mm;          
      }
    
    }

    /*   to show/hide page number in the margin boxes  */

    body {
        --color-margins: orange;
    }


    .pagedjs_margin-top-left-corner:hover, 
    .pagedjs_margin-top-right-corner:hover,
    .pagedjs_margin-bottom-left-corner:hover, 
    .pagedjs_margin-bottom-right-corner:hover,

    .pagedjs_margin-top-left:hover,
    .pagedjs_margin-top-center:hover,
    .pagedjs_margin-top-right:hover,

    .pagedjs_margin-bottom-left:hover, 
    .pagedjs_margin-bottom-center:hover,
    .pagedjs_margin-bottom-right:hover,
    
    .pagedjs_margin-left-top:hover, 
    .pagedjs_margin-left-middle:hover,
    .pagedjs_margin-left-bottom:hover, 
    
    .pagedjs_margin-right-top:hover,
    .pagedjs_margin-right-middle:hover,
    .pagedjs_margin-right-bottom:hover {
        background: grey;
    }
`
