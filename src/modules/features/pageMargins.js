import {Control} from './class.js'


let pageMargins = new Control;

pageMargins.name = 'pageMargins';

pageMargins.render = `<label for="marginTop">margin-top</label>
    <input class="pagedPreviewer-marginUpdate" id="margin-top" name="marginTop" value="10" data-sizing="mm">
    <label for="marginRight">margin-right</label>
    <input class="pagedPreviewer-marginUpdate" id="margin-right" name="marginRight" value="10" data-sizing="mm">
    <label for="marginBottom">margin-bottom</label>
    <input class="pagedPreviewer-marginUpdate" id="margin-bottom" name="marginBottom" value="10" data-sizing="mm">
    <label for="marginLeft">margin-left</label>
    <input class="pagedPreviewer-marginUpdate" id="margin-left" name="marginLeft" value="10" data-sizing="mm">`

export default pageMargins;


