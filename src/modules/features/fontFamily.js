import {Control} from './class.js'

let fontFamily = new Control;

fontFamily.name = 'fontFamily';

fontFamily.render = `
<label for="fontFamily">font-family</label>
<select id="fontFamily" name="fontfamily" data-css-prop="--fontBody">
    <option value="Spectral" selected>Spectral</option>
    <option value="'Courier Prime'">Courier Prime</option>
    <option value="sans-serif">sans serif</option>
</select>`

export default fontFamily;
