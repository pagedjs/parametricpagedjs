import {Control} from './class.js'

let pageSize = new Control;

pageSize.name = 'pageSize';

pageSize.render = `<label class="pagedSize" for="page-size">page size</label>
<select name="page-size" id="page-size" class="pagedSize">
    <option value="A4">A4</option>
    <option value="A5">A5</option>
    <option value="letter">Letter</option>
    <option value="6in 9in">atla</option>
    <option value="200mm 200mm">20cm × 20cm</option>
</select>`;


export default pageSize;
