import {Control} from './class.js'

let lineHeight = new Control;

lineHeight.name = 'lineHeight';

lineHeight.render = `  <div class="pagedPreviewer-item">
<label for="lineHeight">line-height</label>
<input class="cssVarUpdate" id="lineHeight" step="0.1" name="lineHeight" data-css-prop="--lineHeight" type="range" min=".8" max="3"  >
</div>`;


export default lineHeight;
