import {Control} from "./class.js";

let fontSize = new Control();

fontSize.name = "fontSize";

fontSize.render = `
<label for="fontSize">font-size</label>
<input class="cssVarUpdate" id="fontSize" name="fontSize" data-css-prop="--fontSize" type="range" min="12" max="20" data-sizing="px">
`;

export default fontSize;
