class Control {
  constructor(name, render) {
    this.name = name;
    this.render = render;
  }

  updateControl() {
    this.addEventListener("change", console.log(value));
  }
}




function alligator() {
// localize inputs for the style sheet
const inputs = document.querySelectorAll(".cssVarUpdate");
const selects = document.querySelectorAll("select");

//reload on value changes in the input
inputs.forEach((input) => {
  input.addEventListener("change", populatePagedMaker);
});

//reload on value changes in the selects
selects.forEach((select) => {
  select.addEventListener("change", populatePagedMaker);
});

// reload when changing the margins
document.querySelectorAll(".pagedPreviewer-marginUpdate").forEach((mr) => {
  mr.addEventListener("change", populatePagedMaker);
});

// button to restart the preview (if bug)
document
  .querySelector(".pagedPreviewer-button-preview")
  .addEventListener("click", populatePagedMaker);
document
  .querySelector(".pagedPreviewer-button-print")
  .addEventListener("click", print);
}


export  { Control, alligator };