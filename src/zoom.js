class zooming extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered(pages) {
    createInterface();

    var ctm = [1, 0, 0, 1, 0, 0];
    function scalePage(s, x, y, cx, cy) {
      if (!s) s = sr;
      if (typeof x == "undefined" || typeof y == "undefined") {
        var c = getCenterPos();
        x = c[0];
        y = c[1];
      }
      if (typeof cx == "undefined" || typeof cy == "undefined") {
        var c = getCenterPos(true);
        cx = c[0];
        cy = c[1];
      }
      var im = getInverseMatrix(ctm);
      var ox = im[0] * x + im[2] * y + im[4];
      var oy = im[1] * x + im[3] * y + im[5];

      let pages = document.querySelector(".pagedjs_pages");
      if (pages) {
        let wSize = getScrollPage();
        let pWidth = pages.offsetWidth;
        let pHeight = pages.offsetHeight;
        pWidth *= s;
        pHeight *= s;
        ctm[0] = ctm[3] = s;
        if (pWidth < wSize[0]) ctm[4] = (wSize[0] - pWidth) / 2;
        else ctm[4] = 0;
        if (pHeight < wSize[1]) ctm[5] = (wSize[1] - pHeight) / 2;
        else ctm[5] = 0;

        let ps = pages.style;
        ps.transform = `matrix(${ctm[0]},${ctm[1]},${ctm[2]},${ctm[3]},${ctm[4]},${ctm[5]})`;
        ps.margin = "0px";
        ps.padding = "0px";
        let bs = document.body.style;
        bs.width = `${Math.max(pWidth, wSize[0])}px`;
        bs.height = `${Math.max(pHeight, wSize[1])}px`;
        bs.margin = "0px";
        bs.padding = "0px";

        let srs = getScrollRange();
        let sps = getScrollPage();
        let cs = getClientSize();
        let mx = srs[0] - sps[0];
        let my = srs[1] - sps[1];
        if (mx > 0) {
          let nx = ctm[0] * ox + ctm[2] * oy + ctm[4];
          let px = (sps[0] / cs[0]) * cx;
          nx -= px;
          if (nx > 0) document.documentElement.scrollLeft = nx;
        }
        if (my > 0) {
          let ny = ctm[1] * ox + ctm[3] * oy + ctm[5];
          let py = (sps[1] / cs[1]) * cy;
          ny -= py;
          if (ny > 0) document.documentElement.scrollTop = ny;
        }
      }
      let editable = document.querySelectorAll(
        ".pagedjs_page .pagedjs_area > div"
      );
      let zv = document.getElementById("zoomValue");
      zv.innerHTML =
        "Pages: " + editable.length + ", Zoom: " + Math.round(s * 100) + "%";
    }
    var sr = 1.0;
    function pageZoomIn() {
      if (sr < 1.5) sr += 0.1;
      else if (sr < 3.0) sr += 0.5;
      else if (sr < 5.0) sr += 1.0;
      else return false;
      return true;
    }
    function pageZoomOut() {
      if (sr > 3.0) sr -= 1.0;
      else if (sr > 1.51) sr -= 0.5;
      else if (sr > 0.25) sr -= 0.1;
      else return false;
      return true;
    }
    function getInverseMatrix(m) {
      //please refer to: https://github.com/deoxxa/transformation-matrix-js/blob/master/src/matrix.js
      var dt = m[0] * m[3] - m[1] * m[2];
      var a = m[3] / dt;
      var b = -m[1] / dt;
      var c = -m[2] / dt;
      var d = m[0] / dt;
      var e = (m[2] * m[5] - m[3] * m[4]) / dt;
      var f = -(m[0] * m[5] - m[1] * m[4]) / dt;
      return [a, b, c, d, e, f];
    }
    function getBrowserZoom() {
      return window.outerWidth / window.innerWidth;
    }
    function getScrollRange() {
      var sx,
        sy,
        b = document.body,
        r = document.documentElement;
      sx = r.scrollWidth || b.scrollWidth || b.offsetWidth || 0;
      sy = r.scrollHeight || b.scrollHeight || b.offsetHeight || 0;
      return [sx, sy];
    }
    function getScrollPage() {
      var sx,
        sy,
        r = document.documentElement;
      sx = r.clientWidth || r.offsetWidth;
      sy = r.clientHeight || r.offsetHeight;
      return [sx, sy];
    }
    function getScrollPos() {
      if (window.pageYOffset != undefined) return [pageXOffset, pageYOffset];

      var sx,
        sy,
        r = document.documentElement,
        b = document.body;
      sx = r.scrollLeft || b.scrollLeft || 0;
      sy = r.scrollTop || b.scrollTop || 0;
      return [sx, sy];
    }
    function getCenterPos(c) {
      if (!c) {
        var p = getScrollPage();
        var o = getScrollPos();
        return [p[0] / 2 + o[0], p[1] / 2 + o[1]];
      }

      let cs = getClientSize();
      return [cs[0] / 2, cs[1] / 2];
    }
    function getClientSize() {
      var sx,
        sy,
        r = document.documentElement,
        b = window,
        s = screen;
      sx = r.clientWidth || b.innerWidth || s.width;
      sy = r.clientHeight || b.innerHeight || s.height;
      return [sx, sy];
    }
    function preventEvent(e) {
      if (e.preventDefault) e.preventDefault();
      e.returnValue = false;
    }
    function onKeyDown(e) {
      if (
        e.ctrlKey == true &&
        (e.keyCode == 61 ||
          e.keyCode == 107 ||
          e.keyCode == 109 ||
          e.keyCode == 173 ||
          e.keyCode == 187 ||
          e.keyCode == 189)
      ) {
        preventEvent(e);
        if (e.keyCode == 61 || e.keyCode == 107 || e.keyCode == 187) {
          if (!pageZoomIn()) return false;
        } else {
          if (!pageZoomOut()) return false;
        }
        scalePage();
        return false;
      }
    }
    function onMouseWheel(e) {
      if (e.ctrlKey == true) {
        preventEvent(e);
        if (e.wheelDelta > 0) {
          if (!pageZoomIn()) return false;
        } else {
          if (!pageZoomOut()) return false;
        }
        scalePage(sr, e.pageX, e.pageY, e.clientX, e.clientY);
        return false;
      }
    }
    !(function () {
      window.addEventListener("mousewheel", onMouseWheel, { passive: false });
      document.body.addEventListener("keydown", onKeyDown, { passive: false });
    })();
  }
}

Paged.registerHandlers(zooming);

function createInterface() {
  const status = document.createElement("div");
  status.classList.add("statusBar");
  const zoomValue = document.createElement("span");
  zoomValue.id = "zoomValue";
  status.appendChild(zoomValue);
  document.body.appendChild(status);
}
