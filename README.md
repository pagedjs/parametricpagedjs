# BookMaker, the simplest way to add paginated preview of your website

## What it does

Pagedmaker is a print preview solution for any website, based on paged.js.

- Print preview: simulate print preview, but on screen: check how your website will be rendered on paper
- Parametric mode: add control for parametric printing: change font, font-size, colors, and margins

> But isn’t that what firefox and chromium preview offer?

Eventually, that should be the case, when they’ll be done with video, webGL, and all the other features likely to be added to the browser, i’m pretty sure they’ll end up looking for pagedmedia specifications. But until that happens, if you want to have a proper print css preview, you need to print the file, check things, and do it again. 
And yet, you won’t have access to automatic running heads, table of contents, page numbers and cross references, things that we can already use today with paged.js. Check the future of #cssPrint and try paged.js.


## How does it work

1. pagedjs creates an iframe
2. clone the `body` element to the iframe
3. link a css print file (pagedmaker is provided with a basic css file)
4. add Pagedjs previewer to the iframe and runs it
5. the controls in the modal change the data/values for each control



## use and configuration

you can use pagedPreviewer by adding this `pagedPreviewer.js` to your HTML (make sure its type is module).

```
  <script type="module" src="../dist/main.js"></script>
```

and you’ll have the basic overview of the body you’ll paginate.

You can also have a custom configuration for your pagedjs needs`: from custom stylesheet to select the controls of the interface in parametric mode. 

```html
    <!--   <script>
            window.pagedMakerConfig = {
                content: ".bookContent",
                stylesheet: "css/pagedtest.css",
                button: ".pagedjsButton",
                features: [
                     "pagedSize",
                     "pagedMargins",
                    "fontFamily",
                    "fontSize",
                    "fontLineHeight",
                    "backgroundColor",
                    "textColor"
                ],
                uicolor: "orange"
            }
        </script>-->

```

where you can add the following.  WARNING: if you want to use this custom make sure to use all the config for now (i’ll improve that asap). To get the default of each of the option, set it as `undefined`.

`content`: is the dom element you want to paginate (default is the body);
`stylesheet`: a custom stylesheet you want to use (default will use the the system fonts)
`button`: the button element you want to use to start the script; (default will create a new one);
`features`: the control you want to give to your user. Can be one of more of the following list: , `"pagedSize"`, `"pagedMargins"`, `"fontFamily"`, `"fontSize"`, `"fontLineHeight"`, `"backgroundColor"`, `"textColor`";
`uicolor`: the color for the left column of the UI;


### styles pagedjs buttons and modal

Use those css classes in your CSS: 
`.pagedPreviewer-button-modal` to change the button that call the modal. It will be added as the first element of the body element,
`.pagedPreviewer-modal` to change the styles of the modal (warning, it’s using css-grid to make things work).


## TODO 

- make it work without server: remove link to css file: fetch the content of the css files and create one simple style element so nothing get fetched. Warn the user that the 
- Create a modular way to add UI element using json.


